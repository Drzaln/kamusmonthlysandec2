package com.develop.uniq.kamussandec.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.develop.uniq.kamussandec.Model.ModelKamus;

import java.util.ArrayList;

public class DataHelper {
    private static String English = Config.DATABASE_TABLE_ENGLISH;
    private static String Indonesia = Config.DATABASE_TABLE_INDONESIA;

    private Context context;
    private DbHelper dbHelper;
    private SQLiteDatabase sqLiteDatabase;

    public DataHelper(Context context){
        this.context = context;
    }

    public DataHelper open() throws SQLException{
        dbHelper = new DbHelper(context);
        sqLiteDatabase = dbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        dbHelper.close();
    }

    public Cursor searchQueryByName(String query, boolean english){
        String DATABASE_TABLE = english ? English : Indonesia;
        return sqLiteDatabase.rawQuery("SELECT * FROM "+DATABASE_TABLE + " WHERE " + Config.DATABASE_FIELD_WORD + " LIKE '%" + query.trim() + "%'", null);
    }

    public ArrayList<ModelKamus>getDataByname(String search, boolean english){
        ModelKamus kamusModel;
        ArrayList<ModelKamus>arrayList = new ArrayList<>();
        Cursor cursor = searchQueryByName(search, english);
        cursor.moveToFirst();
        if (cursor.getCount() > 0){
            do {
                kamusModel = new ModelKamus();
                kamusModel.setId(cursor.getInt(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_ID)));
                kamusModel.setWord(cursor.getString(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_WORD)));
                kamusModel.setTranslate(cursor.getString(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_TRANSLATE)));
                arrayList.add(kamusModel);
                cursor.moveToNext();
            }while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public Cursor queryAllData(boolean english){
        String DATABASE_TABLE = english ? English : Indonesia;
        return sqLiteDatabase.rawQuery("SELECT * FROM " + DATABASE_TABLE + " ORDER BY " + Config.DATABASE_FIELD_ID + " ASC", null);
    }

    public ArrayList<ModelKamus>getDataALL(boolean english){
        ModelKamus kamusModel;
        ArrayList<ModelKamus>arrayList = new ArrayList<>();
        Cursor cursor = queryAllData(english);
        cursor.moveToFirst();
        if (cursor.getCount() > 0){
            do{
                kamusModel = new ModelKamus();
                kamusModel.setId(cursor.getInt(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_ID)));
                kamusModel.setWord(cursor.getString(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_WORD)));
                kamusModel.setTranslate(cursor.getString(cursor.getColumnIndexOrThrow(Config.DATABASE_FIELD_TRANSLATE)));
                arrayList.add(kamusModel);
                cursor.moveToNext();
            }while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public long insert (ModelKamus kamusModel, boolean english){
        String DATABASE_TABLE = english ? English : Indonesia;
        ContentValues initialValues = new ContentValues();
        initialValues.put(Config.DATABASE_FIELD_WORD, kamusModel.getWord());
        initialValues.put(Config.DATABASE_FIELD_TRANSLATE, kamusModel.getTranslate());
        return sqLiteDatabase.insert(DATABASE_TABLE, null, initialValues);
    }

    public void insertTransaction (ArrayList<ModelKamus>kamusModel, boolean english){
        String DATABASE_TABLE = english ? English : Indonesia;
        String sql = "INSERT INTO " + DATABASE_TABLE + " (" + Config.DATABASE_FIELD_WORD + ", " + Config.DATABASE_FIELD_TRANSLATE + ") VALUES (?, ?)";
        sqLiteDatabase.beginTransaction();

        SQLiteStatement statement = sqLiteDatabase.compileStatement(sql);
        for (int i = 0; i < kamusModel.size(); i++){
            statement.bindString(1, kamusModel.get(i).getWord());
            statement.bindString(1, kamusModel.get(i).getTranslate());
            statement.clearBindings();
        }
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }
}
