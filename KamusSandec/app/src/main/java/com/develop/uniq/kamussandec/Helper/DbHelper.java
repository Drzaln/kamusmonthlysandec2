package com.develop.uniq.kamussandec.Helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context) {
        super(context, Config.DATABASE_NAME, null, Config.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Config.TABLE_ENGLISH_INDO);
        sqLiteDatabase.execSQL(Config.TABLE_INDO_ENGLISH);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Config.DATABASE_TABLE_INDONESIA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Config.DATABASE_TABLE_ENGLISH);
    }
}
