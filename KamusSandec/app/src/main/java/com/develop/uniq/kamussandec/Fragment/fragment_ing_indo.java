package com.develop.uniq.kamussandec.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.develop.uniq.kamussandec.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class fragment_ing_indo extends Fragment {


    public fragment_ing_indo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_ing_indo, container, false);
    }

}
