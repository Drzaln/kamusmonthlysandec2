package com.develop.uniq.kamussandec.Helper;

public final class Config {
    //nama dan versi database
    public static final String DATABASE_NAME = "kamusingid.db";
    public static final int DATABASE_VERSION = 1;

    //tabel
    public static String DATABASE_TABLE_ENGLISH = "DATA_ENGLISH";
    public static String DATABASE_TABLE_INDONESIA = "DATA_INDONESIA";

    //field
    public static String DATABASE_FIELD_ID = "ID";
    public static String DATABASE_FIELD_WORD = "WORD";
    public static String DATABASE_FIELD_TRANSLATE = "TRANSLATE";

    //buat tabel
    public static final String TABLE_INDO_ENGLISH = "CREATE TABLE "+Config.DATABASE_TABLE_INDONESIA+"("+Config.DATABASE_FIELD_ID+"integer primary key autoincrement, "+Config.DATABASE_FIELD_WORD+"string, "+Config.DATABASE_FIELD_TRANSLATE+"text not null); ";
    public static final String TABLE_ENGLISH_INDO = "CREATE TABLE "+Config.DATABASE_TABLE_ENGLISH+"("+Config.DATABASE_FIELD_ID+"integer primary key autoincrement, "+Config.DATABASE_FIELD_WORD+"string, "+Config.DATABASE_FIELD_TRANSLATE+"text not null); ";

    //bundle
    public static String BUNDLE_WORD = "word";
    public static String BUNDLE_TRANSLATE = "translate";
}
